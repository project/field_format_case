<?php

/**
 * @file
 * Example implementations for the hooks of this module.
 */

/**
 * Use this for adding types to the list.
 *
 * @param array $types
 *   The supported widget types.
 */
function hook_field_format_case_supported_widget_types_alter(array &$types) {
  $types[] = 'mycustom_textfield';
}

/**
 * Use this to alter the columns for an element not matching its real columns.
 *
 * In some cases there may be different column names in the form than in the
 * field's column configuration. A module where this happens is the
 * select_or_other field. Using the following drupal_alter()
 * we allow other modules to change the $element_columns array so that the
 * settings for the column get properly implemented for every $original_column.
 *
 * The following code is the actual implementation of this hook by this module.
 *
 * @param array $element_columns
 *   The column names as found in the element.
 * @param string $original_column
 *   The original column name as defined in each field's hook_field_schema().
 * @param array $element
 *   The element being processed right now.
 * @param array $context
 *   The context given to hook_field_widget_form_alter().
 */
function hook_field_format_case_element_column_alter(array &$element_columns, $original_column, array $element, array $context) {
  switch ($context['instance']['widget']['type']) {
    case 'select_or_other':
      if ($original_column == 'value') {
        // select_or_other also has a "select" element column which is a
        // <select> and as a result we don't need it here.
        $element_columns = array('other');
      }
      break;
  }
}
