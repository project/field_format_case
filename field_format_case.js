(function($) {
  Drupal.behaviors.fieldFormatCase = {
    attach: function(context, settings) {
      var uppercase = $('.field-format-case-uppercase'),
          lowercase = $('.field-format-case-lowercase');

      uppercase.once('field-format-case').bind('keyup', function() {
        var _ = $(this);
        _.val(Drupal.behaviors.fieldFormatCase.removeAccents(_.val().toUpperCase()));
      });

      lowercase.once('field-format-case').bind('keyup', function() {
        var _ = $(this);
        _.val(_.val().toLowerCase());
      });

      // Values added using Right Click+Paste won't have the keyup event
      // triggered. Instead, we catch these values upon submission.
      $('form').once('field-format-case').bind('submit', function() {
        uppercase.trigger('keyup');
        lowercase.trigger('keyup');
      });
    },
    // @see https://gist.github.com/alisterlf/3490957
    removeAccents: function(str) {
      var accents    = 'ΆÀÁÂÃÄÅάàáâãäåßΉήΌÒÓÔÕÕÖØòóôõöøΈÈÉÊËέèéêëðÇçÐΊÌÍÎÏίìíîïÙÚÛÜùúûüÑñŠšΎŸύÿýŽžΏώ';
      var accentsOut = "ΑAAAAAAαaaaaaaBΗηΟOOOOOOOooooooΕEEEEεeeeeeCcDΙIIIIιiiiiUUUUuuuuNnSsΥYυyyZzΩω";
      str = str.split('');
      var strLen = str.length;
      var i, x;
      for (i = 0; i < strLen; i++) {
        if ((x = accents.indexOf(str[i])) != -1) {
          str[i] = accentsOut[x];
        }
      }
      return str.join('');
    }
  }
})(jQuery);